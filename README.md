# ARQUITECTURA DE COMPUTADORAS - SCD1003 - ISB
## VON-NEUMANN VS HARVARD

``` plantuml

@startmindmap 
* VON-NEUMANN VS HARVARD
 * Son arquitecturas de computadoras
  *_ Estas son
   * MODELOS
    * HARVARD
     * Modelo creado por Howard H. Aiken, que\ntambién dió origen a la computación
     * Tuvo su origen en 1945 y toma nombre\nde la computadora Harvard Mark
     * Almacenaba instrucciones en cintas perforadas y\nlos datos en contadores electromecánicos
     *_ Utiliza
      * Dos tipos de memoria
       * MEMORIA DE INSTRUCCIONES
       * MEMORIA DE DATOS
      * Conectados por buses
     * Esta arquitectura consta de 3 propiedades esenciales
      * Un único espacio de memoria
      * Contenido accesible por posición
      * Ejecución de instrucciones por secuencia
     * Puede ejecutar datos e instrucciones\nal mismo tiempo
     *_ Tiene
      * MICROCONTROLADORES
       * Estos sistemas están encargados de controlar el\nfuncionamiento de los dispositivos
       *_ Se clasifican
        * UNIDAD DE PROCESO
         * Aquí se ejecutan las instrucciones que son pasan por la\nmemoria de instrucciones, estos datos son almacenados
        * E/S
         * Según la aplicación del microcontrolador, son\nnecesarios unos recursos u otros.
      * DSP
       * Un procesador de señales digitales o digital signal\nprocessor (DSP) es un dispositivo capaz de procesar\nen tiempo real señales procedentes de diferentes\nfuentes.
        * CARACTERISTICAS
         * Implementan muchas operaciones por hardware\nque otros procesadores hacen por software
         * Están diseñados para que sean escalables y\npara trabajar en paralelo con otros DSP
         * El hardware del procesador puede ser más complejo que\nel de algunos microcontroladores o microprocesadores
     * No se usa mucho hoy día, lo que hace\naún más dificil su funcionamiento
    * VON-NEUMANN
     * Nació el 28 de Dicicembre en Budapest, Hungría.\nMurió el 8 de Febrero de 1957 en Washington, DC. 
     * Creó la arquitectura de las computadoras actuales
     * El 30 de Julio de 1945 desarrolla un programa que\nal parecer tenía como propósito almacenar\ndatos en una memoria
     *_ Establece
      * Una máquina con ciertos elementos que\nse encuentran interconectados
     *_ Este
      * CONTRIBUYÓ
       * Definición del número ordinal y teoría de conjuntos
       * Desarrollo de técnicas de implosión de la bomba atómica
     *_ También
      * MODIFICÓ
       * La máquina IAS que utilizaba aritmética binaria
       * La ENIAC que utilizaba números decimales
     *_ Propone
      * PROCESADOR
       * Procesa la información, es decir, recibe la información y\ncon ella hace ciertas operaciones para posteriormente\ndesplegar la información hacia afuera
        *_ Se clasifica en
         * UNIDAD ARITMÉTICA\nLÓGICA
          * Procesa datos y envía\nlos registros
         * UNIDAD DE CONTROL
          * Analiza los resultados obtenidos y los\nenvía a otros dispositivos
      * ALMACENAMIENTO
       * Von-Neumann imaginó en su esquema un tipo\nde memoria paredcida a la RAM, paraguardar\ntemporalmente información
      * E/S
       * Se comunican con el computador con ciertos elementos\nque sirvieran como intermediarios, para proporcionar y\nrecibir información del procesador
        *_ Se clasifica en
         * ENTRADA
          * Permiten el ingreso de información\nal conmutador
         * SALIDA
          * Permiten la salida de información\ndel conmutador
         * BUSES
          * Funcionan como conectores, de manera que se\nencargan de conectar los dispositivos de E/S, el\nalmacenamiento y el procesador
      * Todos juntos funcionando correctamente para un\ncorrecto funcionamiento de la computadora
     * Además, tuvo la idea de separar\nel Software del Hardware
@endmindmap
```

## SUPERCOMPUTADORAS EN MÉXICO

``` plantuml

@startmindmap
* SUPERCOMPUTADORAS EN MÉXICO
 *_ QUÉ SON
  * Una supercomputadora es una infraestructura que se diseña\ny construye para procesar grandes cantidades de información
   * EN MÉXICO
    * UNAM
     * Tiene primer supercomputadora en latinoamérica\nel 18 de Julio de 1958
      *_ Fue posible por
       * Porque se tenía un interés en esa tecnología\npara lograr los cálculos
     * IBM-650
      * Leía la información a través de\ntarjetas perforadas
      * Creadas con bulbos que requerían mucho\nespacio y energía
      * Capaz de procesar 1000 operaciones\naritméticas por segundo
     * CRAY-432
      * En 1991, la UNAM se consolida en las grandes ligas\nde la computación al adquirir la CRAY-432
      * Primer supercomputadora en latinoamérica
      * Equivalente a 2000 computadoras de oficina
    * KanBALAM
     * Puede realizar 7 billones de operaciones\nmatemáticas por segundo
     *_ Capacidad
      * Más de 1300 computadoras de escritorio
     * UTILIZADA
      * Para realizar proyectos de investigación como\nastronomía, química cuántica, etc
    * MIZTLI
     * Puesta en operación en 2012
     * Realiza anualmente 120 proyectos de investigación, tanto de la UNAM,\ncomo de otras instituciones que solicitan su servicio
      *_ Tiene
       * Una capacidad de 8344 procesadores
      *_ Contribuye 
       * En el estudio del universo,sismos, farmacos, etc
    * XIUHCOATL
     * Creada en 2012
     * Segunda supercomputadora más rápida\nde latinoamérica
     * Surge de una iniciativa de las 3 instituciones más\nimportantes de educación superior de méxico
      * Constantes actualizaciones la mantienen en\nfuncionamiento
      * Es la segunda supercomputadora más rápida\nen latinoamérica
     * En 2014 se adquiere más equipos
     * En 2015 se fortalece la estructura\ndel cluster
    * BUAP
     * Una de 5 supercomputadoras más poderosas\nde latinoamérica
     * En tan solo un segundo puede ejecutar\n2 billones de operaciones
     *_ Tiene
      * Gran capacidad para simulaciones
     *_ Y un
      * Almacenamiento equivalente a 5000\ncomputadoras portátiles
     * FUNCIONES
      * Se puede recrear la formación de estrellas, avances de\nhuracanes, la estructura del ADN, entre otros
     * PRECAUCIONES
      * Se deben mintorear las temperaturas de las habitaciones,\npara evitar que se calienten por la gran cantidad de\noperaciones que estas realizan
      * En caso de que se rebase la temperatura de los 27 grados\ncentigrados, se manda una alerta para checar que todo\nesté bien
     * Cada año el laboratorio de la BUAP abre una convocatoria para\nquien tenga un proyecto que necesite procesar grandes\ncantidades de información
   * A mediados de los 80 las computadoras dejaron deser tan exclusivas\npara científicos por lo que empezaron a usarse en oficinas y en\nlas casas de las personas
@endmindmap
```

